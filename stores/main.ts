import { defineStore } from 'pinia'
export const useMainStore = defineStore('main', {
	state: () => ({
		counter: 0,
	}),
	// optional getters
	getters: {
		// getters receive the state as first parameter
		doubleCounter: (state) => state.counter * 2,
		// use getters in other getters
		doubleCounterPlusOne(): number {
			return this.doubleCounter + 1
		},
	},
	// optional actions
	actions: {
		reset() {
			// `this` is the store instance
			this.counter = 0
		},
	},
})
